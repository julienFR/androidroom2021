package com.example.roomlp2021;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

class WordViewHolder extends RecyclerView.ViewHolder{
    private final TextView wordItemView;

    public  WordViewHolder(View itemView){
        super(itemView);
        wordItemView = itemView.findViewById(R.id.textView);
    }

    public TextView getWordItemView() {
        return wordItemView;
    }
}