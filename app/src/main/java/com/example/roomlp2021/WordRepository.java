package com.example.roomlp2021;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;

public class WordRepository {
    private WordDao myWordDao;
    private LiveData<List<Word>> myListWords;
    private LiveData<Integer> nbElemLD;

    public WordRepository(Application application){
        WordRoomDatabase db = WordRoomDatabase.getDatabase(application);
        myWordDao = db.wordDao();
        myListWords = myWordDao.getAllWords();
        nbElemLD = myWordDao.nbElementsLD();
    }

    public LiveData<List<Word>> getMyListWords() {
        return myListWords;
    }

    public LiveData<Integer> getNbElemLD() {
        return nbElemLD;
    }

    public void insert(Word word){
        new insertAsyncTask(myWordDao).execute(word);
    }

    private static class insertAsyncTask extends AsyncTask<Word,Void,Void> {
        private WordDao dao;
        public insertAsyncTask(WordDao myWordDao) {
            dao = myWordDao;
        }

        @Override
        protected Void doInBackground(Word... words) {
            dao.insert(words[0]);
            return null;
        }
    }

    public void deleteAll(){
        new deleteAllAsyncTask(myWordDao).execute();
    }

    private static class deleteAllAsyncTask extends AsyncTask<Void,Void,Void>{
        WordDao dao;
        public deleteAllAsyncTask(WordDao myWordDao) {
            dao= myWordDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            dao.deleteAll();
            return null;
        }
    }

    public Integer getNbElements(){
        try {
            return new getNbElementsAsync(myWordDao).execute().get();
        }catch(Exception e){
            Log.d("MesLogs","pb getNbElements");
        }
        return null;
    }

    private static class getNbElementsAsync extends AsyncTask<Void,Void,Integer>{
        WordDao dao;
        public getNbElementsAsync(WordDao myWordDao) {
            dao = myWordDao;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            return new Integer(dao.nbElements());
        }
    }
}
